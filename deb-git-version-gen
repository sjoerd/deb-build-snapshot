#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright © 2015-2018 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# deb-git-version-gen — Make up a version number for a package
#
# Assumptions:
# * dpkg-dev, git, python3, python3-debian are available
# * The current working directory is the root of a git repository
# * The current branch has upstream sources and a debian/ directory
# * Debian packaging tags look like debian/1.2.3-4 (or vendor/1.2.3-4vendor5)
#
# and with --upstream:
# * Upstream tags look like v1.2.3 or 1.2.3

import argparse
import json
import logging
import os
import shlex
import subprocess
import sys
import time

try:
    import typing
except ImportError:
    pass
else:
    typing  # silence "unused" warnings

# python3-debian or python-debian is available in Debian, Fedora, etc.
from debian.debian_support import Version


logger = logging.getLogger('deb-git-version-gen')


def check_output(command, *args, **kwargs):
    logger.debug('%s', command)
    return subprocess.check_output(command, *args, **kwargs)


def check_call(command, *args, **kwargs):
    logger.debug('%s', command)
    return subprocess.check_call(command, *args, **kwargs)


class Failure(Exception):
    """An exception that does not normally provoke a traceback"""


class Versioner:
    def __init__(
            self,
            *,
            branch_marker=None,         # type: typing.Optional[str]
            date_based=False,
            dch=False,
            debug=False,
            release=True,               # type: typing.Optional[bool]
            upstream=True
    ):
        # type: (...) -> None
        self.branch_marker = branch_marker
        self.date_based = date_based
        self.dch = dch
        self.debug = debug
        self.release = release
        self.upstream = upstream

    def parse_description(self, description):
        # type: (str) -> typing.Tuple[str, Version, int, str]
        """Parse `git describe --long` output.

        Return the name of the tag we used as a reference point,
        the Version that it represents, the number of commits since
        that tag to get to where we are now, and the short commit hash
        of where we are now.
        """
        try:
            return self._parse_description(description)
        except (ValueError, Failure) as e:
            raise Failure(
                'Unable to parse `git describe` output {!r}: {}'.format(
                    description, str(e)))

    def _parse_description(self, description):
        # type: (str) -> typing.Tuple[str, Version, int, str]

        logger.debug('parsing description: %s', description)

        # (VVV, [WW,] NN, gCCCCCCC)
        parts = description.split('-')
        if len(parts) < 3:
            raise Failure('Number of dash-separated parts is less than 3')
        # VVV[-WW]
        tag = '-'.join(parts[:-2])

        if '/' in tag:
            # Chop off debian/ resulting in VVV[-WW]-NN-gCCCCCCC.
            # If the version is like debian/openarena-textures/0.8.5split-11,
            # cope with that too.
            tagged_version = tag.rsplit('/', 1)[1]
        elif tag.startswith('v'):
            tagged_version = tag[1:]
        else:
            tagged_version = tag

        # undo gbp version mangling: 1%1.2_beta3 -> 1:1.2~beta3
        tagged_version = tagged_version.replace('%', ':').replace('_', '~')
        # NN
        counter = parts[-2]
        # CCCCCCC
        commit = parts[-1].lstrip('g')

        return tag, Version(tagged_version), int(counter), commit

    def count_all_commits(self):
        # type: () -> typing.Tuple[int, str]
        """Fallback mode if we can't find a tag: count all the commits since
        the beginning of history.

        Return the number of commits that have ever happened, and the
        short commit hash of the current commit. This is analogous to
        the last two return values from parse_description(), if we
        imagine that the tag found by `git describe` was just before the
        first commit to this repository (which of course can't happen).
        """
        counter = int(
            check_output(
                'git rev-list HEAD | wc -l',
                shell=True,
                universal_newlines=True).strip())
        commit = check_output(
            ['git', 'show', '--pretty=format:%h', '-s'],
            universal_newlines=True).strip()
        return counter, commit

    def main(self):
        changelog_version = Version(
            check_output([
                'dpkg-parsechangelog', '-SVersion', '-n1'
            ], universal_newlines=True).strip('\n')
        )
        is_native = (changelog_version.debian_revision is None)

        if os.path.exists('debian/source/format'):
            source_format = open('debian/source/format').read().strip()
        else:
            source_format = '1.0'

        if source_format not in ('3.0 (quilt)', '3.0 (native)', '1.0'):
            raise Failure(
                'debian/source/format unsupported: {}'.format(source_format))

        if source_format == '3.0 (native)' and not is_native:
            raise Failure(
                'debian/source/format is native but version number is not')
        elif source_format == '3.0 (quilt)' and is_native:
            raise Failure(
                'debian/source/format is not native but version number is')

        release = False

        if self.debug:
            diag_sink = sys.stderr
        else:
            diag_sink = subprocess.DEVNULL

        if self.upstream and not is_native:
            logger.debug('We are the upstream for this non-native package')
            # vVVV[-WW]-NN-gCCCCCCC
            try:
                raw_description = check_output([
                    'git', 'describe', '--long', '--tags',
                    '--match=[v0-9]*',
                ], universal_newlines=True, stderr=diag_sink).strip()
            except subprocess.CalledProcessError:
                tag = None
                tagged_version = None
                counter, commit = self.count_all_commits()
                reference_point = 'beginning of history'
                logger.debug(
                    '%s is %d commits since beginning of history',
                    commit, counter)
            else:
                tag, tagged_version, counter, commit = self.parse_description(
                    raw_description)
                reference_point = tag
                logger.debug(
                    '%s is %d commits since tag %s (version %s)',
                    commit, counter, tag, tagged_version)

            if counter == 0:
                logger.debug(
                    'Exactly at an upstream tag, presumably trying to '
                    'release it')
                is_upstream = True
            elif tagged_version is None:
                logger.debug(
                    'There are no tags, so we will have to do an upstream '
                    'release before we can do a downstream release')
                is_upstream = True
            else:
                assert tag is not None
                logger.debug(
                    'We are after an upstream tag. Checking whether the '
                    'differences are packaging-only or upstream...')
                is_upstream = False

                diffstat = check_output([
                    'git', 'diff', '--name-only', '--no-renames', '-z',
                    tag + '..HEAD',
                ], universal_newlines=True)
                for line in diffstat.split('\0'):
                    if line and not line.startswith('debian/'):
                        logger.debug('Upstream change found')
                        is_upstream = True
                        break
                else:
                    logger.debug('Only packaging changes found')
        else:
            is_upstream = False
            logger.debug('This is a packaging revision or native package')

        if is_native or not is_upstream:
            logger.debug('Looking for most recent packaging tag')
            offset = 0
            while offset < 10:
                tagged_version = check_output([
                    'dpkg-parsechangelog', '-SVersion',
                    '-o{}'.format(offset), '-n1',
                ], universal_newlines=True).strip('\n')
                # ~ and : are necessary syntax elements in some dpkg
                # version numbers but are not valid in git tags;
                # convert them in the same way as gbp,
                # 1:1.2~beta3 -> 1%1.2_beta3
                mangled_version = tagged_version.replace(':', '%')
                mangled_version = mangled_version.replace('~', '_')
                try:
                    raw_description = check_output([
                        'git', 'describe', '--tags',
                        '--match=*/' + mangled_version,
                        '--long',
                    ], universal_newlines=True, stderr=diag_sink).strip()
                except subprocess.CalledProcessError:
                    try:
                        raw_description = check_output([
                            'git', 'describe',
                            '--tags', '--match=' + mangled_version,
                            '--long',
                        ], universal_newlines=True, stderr=diag_sink).strip()
                    except subprocess.CalledProcessError:
                        logger.debug('Not found, trying previous release')
                        offset += 1
                        continue

                _, tagged_version, counter, commit = self.parse_description(
                    raw_description)
                reference_point = str(tagged_version)
                logger.debug(
                    '%s is %d commits since packaging tag for version %s',
                    commit, counter, tagged_version)
                break
            else:
                # No recent Debian version has a tag, fall back to counting
                # every commit we've ever done (as though there was a tag
                # just before the beginning of history)
                tagged_version = None
                counter, commit = self.count_all_commits()
                reference_point = 'beginning of history'
                logger.debug(
                    '%s is %d commits since beginning of history',
                    commit, counter)

        if tagged_version is None:
            logger.debug(
                'Nothing has been tagged yet: making sure version is '
                'slightly before latest changelog entry %s',
                changelog_version)
            # Nothing has been tagged yet, and the changelog says something
            # like "foo (vvv[-ww]) UNRELEASED; urgency=low". Choose a
            # version that is not only less than vvv[-ww], but also less
            # than what would appear if we did have a tag for version
            # VVV[-WW] < vvv available, by appending '~' (which sorts
            # before the empty string), twice (so it sorts before '~').
            snapshot_version = Version(changelog_version)

            if is_upstream or is_native:
                snapshot_version.upstream_version += '~~'
            else:
                snapshot_version.debian_version += '~~'
        else:
            # VVV[-WW]
            snapshot_version = Version(tagged_version)

        if snapshot_version.epoch is None:
            logger.debug(
                'Using epoch %s from changelog',
                changelog_version.epoch)
            snapshot_version.epoch = changelog_version.epoch

        release = self.release

        if release is None:
            logger.debug(
                'Automatically determining whether this is a release... %s',
                str(counter == 0))
            release = (counter == 0)

        if release and counter != 0:
            raise Failure(
                'Cannot release: {} is {} commits after {}'.format(
                    commit, counter, reference_point))

        if self.date_based:
            snapshot_marker = '+{}+g{}'.format(
                time.strftime('%Y%m%d.%H%M%S', time.gmtime()),
                commit,
            )
        else:
            snapshot_marker = '+{}+g{}'.format(counter, commit)

        if self.branch_marker:
            snapshot_marker = self.branch_marker + snapshot_marker

        if counter == 0 and release and not self.branch_marker:
            # we are exactly at a tag and trying to release: use it
            if changelog_version.upstream_version == \
                    snapshot_version.upstream_version:
                # we are exactly at an upstream tag and the changelog
                # describes a corresponding Debian revision: go with that
                snapshot_version = changelog_version
        elif is_upstream or is_native:
            # VVV+NN+gCCCCCCC-0~snapshot
            snapshot_version.upstream_version = '{}{}'.format(
                    snapshot_version.upstream_version, snapshot_marker)
            if not is_native:
                # we need some sort of Debian revision number, and the one
                # in the changelog is meaningless because we've bumped the
                # upstream version
                snapshot_version.debian_revision = '0~snapshot'
        elif snapshot_version.debian_revision is None:
            # Rare special case: we are going from a native version
            # VVV to a non-native version VVV-WW. Use VVV-0+NN+gCCCCCCC,
            # as though the native package had been a non-native package
            # with Debian revision 0
            snapshot_version.debian_revision = '0{}'.format(snapshot_marker)
        else:
            # VVV-WW+NN+gCCCCCCC
            snapshot_version.debian_revision = '{}{}'.format(
                    snapshot_version.debian_revision, snapshot_marker)

        if (tagged_version is not None and
                changelog_version.upstream_version
                != Version(tagged_version).upstream_version):
            # git says the latest tag is foo_1.2 but debian/changelog says
            # we are foo_2.0-1, or something. This probably means someone
            # is preparing to make a release with new API/ABI.
            # Make the snapshot version "slightly less than" the version
            # being prepared, for example 2.0~1.2+3+g567890a instead of
            # 1.2+3+g567890a, so that it satisfies backport-friendly
            # dependencies like foo (>= 2.0~).
            # Conveniently, 2.0~1.2+... is less than 2.0~beta1, so
            # genuine prereleases will normally sort higher than snapshots.
            if is_native or is_upstream:
                snapshot_version.upstream_version = '{}~{}'.format(
                    changelog_version.upstream_version,
                    snapshot_version.upstream_version)
            else:
                # We have to leave the upstream version as it is, otherwise
                # we'd have the wrong orig tarball; make the Debian revision
                # small instead, like 2.0-0~1.2+0vendor1+3+g567890a
                # (which we want to be less than 2.0-0vendor1)
                snapshot_version.debian_revision = '0~{}+{}'.format(
                    snapshot_version.upstream_version,
                    snapshot_version.debian_revision).replace('-', '+')
                snapshot_version.upstream_version = \
                    changelog_version.upstream_version
        elif (tagged_version is not None and
                (changelog_version.upstream_version ==
                 Version(tagged_version).upstream_version) and
                changelog_version.debian_revision is not None and
                Version(tagged_version).debian_revision is not None and
                (changelog_version.debian_revision !=
                 Version(tagged_version).debian_revision)):
            # git says the latest tag is foo_1.2-3 but debian/changelog says
            # we are foo_1.2-4. Make sure we satisfy foo (>= 1.2-4~)
            snapshot_version.debian_revision = '{}~{}'.format(
                changelog_version.debian_revision,
                snapshot_version.debian_revision).replace('-', '+')
            snapshot_version.upstream_version = \
                changelog_version.upstream_version

        if '/' in str(snapshot_version):
            raise Failure('Bad version {!r}'.format(snapshot_version))

        ret = dict(
            changelog_version=str(changelog_version),
            commit=commit,
            counter=counter,
            is_native=is_native,
            is_upstream=is_upstream,
            snapshot_version=str(snapshot_version),
        )

        if tagged_version is not None:
            ret['tagged_version'] = str(tagged_version)

        # If the version in the changelog is not already what we want,
        # make it so
        if changelog_version != snapshot_version:
            dch_argv = [
                'env', 'DEBFULLNAME=Snapshot',
                'DEBEMAIL=snapshot@localhost',
                'dch',
                '--no-conf',        # must be first, do not re-order
                '--newversion', str(snapshot_version),
                '--allow-lower-version', '.*',
                '--release-heuristic', 'changelog',
                'snapshot: commit {} ({} commits after {})'.format(
                    commit, counter, reference_point),
            ]

            if self.dch:
                check_call(dch_argv)
            else:
                ret['snapshot_message'] = dch_argv[-1]
                ret['dch'] = ' '.join(shlex.quote(a) for a in dch_argv)

        return ret


def main():
    # type: () -> None

    logging.getLogger().setLevel(logging.INFO)

    if sys.stderr.isatty():
        try:
            import colorlog
        except ImportError:
            pass
        else:
            formatter = colorlog.ColoredFormatter(
                '%(log_color)s%(levelname)s:%(name)s:%(reset)s %(message)s')
            handler = logging.StreamHandler()
            handler.setFormatter(formatter)
            logging.getLogger().addHandler(handler)

    # This is a no-op if we already attached a (coloured log) handler
    logging.basicConfig()

    parser = argparse.ArgumentParser(
        description='Make up a version number for a package')

    parser.add_argument(
        '--packaging', '--packaging-only', '-p',
        help='assume that we are only packaging this package (default)',
        action='store_false', dest='upstream', default=False)
    parser.add_argument(
        '--upstream', '-u',
        help='assume that we are the upstream for this package',
        action='store_true', default=False)
    parser.add_argument(
        '--release', help='make a real release, not a snapshot',
        action='store_true', default=False)
    parser.add_argument(
        '--guess-release',
        help='make a real release if we are currently at a tag',
        action='store_const', const=None, dest='release')
    parser.add_argument(
        '--dch', '--debchange',
        help='run debchange to update debian/changelog',
        action='store_true', default=False)

    parser.add_argument(
        '--counter-based',
        help='base version numbers on commit count (default)',
        action='store_false', dest='date_based', default=False)
    parser.add_argument(
        '--date-based', help='base version numbers on build date',
        action='store_true', default=False)

    parser.add_argument(
        '--branch-marker', help='add this string to snapshot marker',
        default=None)

    parser.add_argument(
        '--json',
        help='produce JSON output (default: just the snapshot version)',
        action='store_true')

    parser.add_argument(
        '--debug',
        help='Give more diagnostics',
        action='store_true')

    args = parser.parse_args()

    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    try:
        version_info = Versioner(
            branch_marker=args.branch_marker,
            date_based=args.date_based,
            dch=args.dch,
            debug=args.debug,
            release=args.release,
            upstream=args.upstream,
        ).main()
    except (Failure, subprocess.CalledProcessError) as e:
        if args.debug:
            raise
        else:
            logger.error('%s', e)
            sys.exit(1)

    if args.json:
        json.dump(version_info, sys.stdout)
        print('')
    else:
        print(version_info['snapshot_version'])


if __name__ == '__main__':
    main()
